from django import forms
from django.contrib import messages
import re

def validation_for_empty(value):
  chs = ['#', '@', '&', '%', '^', '*']
  if value.strip() == "":
      raise forms.ValidationError("Empty value not allowed")
  for ch in chs:
      if ch in value:
          raise forms.ValidationError("special characters are not allowed")

def validation_for_alphanumeric(value):
    m = re.match(r"^[^0-9]\w+",value)
    if m:
        return m.group(0)
    else:
        raise forms.ValidationError("Not alphanumeric")

def validation_for_phone_number(value):
    m = re.match(r"^[\d +]+$",value)
    if m:
        return m.group(0)
    else:
        raise forms.ValidationError("Not valid phone number")

def validation_for_only_numeric(value):
    m = re.match(r"^\d+$",value)
    if m:
        return m.group(0)
    else:
        raise forms.ValidationError("Not valid number")

def validation_for_only_character(value):
    m = re.match(r"^[^ _+!]+[a-zA-Z_ ]+$",value)
    if m:
        return m.group(0)
    else:
        raise forms.ValidationError("Not valid characters")

def validation_for_percent_number(value):
    m = re.match(r"^[0-9]{1,2}[%]$|(100%)",value)
    if m:
        return m.group(0)
    else:
        raise forms.ValidationError("Not valid percent")