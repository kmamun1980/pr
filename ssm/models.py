from django.db import models
#from model_utils.fields import StatusField, MonitorField
from model_utils import Choices
from django.contrib.auth.models import User
from datetime import date
from django.contrib import messages
from ssm.validations import *
# Create your models here.

PLAN_FREQUENCY = (
    ("monthly", "Monthly"),
    ("quarterly", "Quarterly"),
    ("halfyearly", "Half-yearly"),
    ("yearly", "Yearly"),
)

class Country(models.Model):
    country_name = models.CharField(max_length=50, blank=False)

    def __unicode__(self):
        return self.country_name

    class Meta:
        verbose_name_plural = 'Country'


class State(models.Model):
    country = models.ForeignKey(Country)
    state_name = models.CharField(max_length=50, blank=False)
    state_abbr = models.CharField(max_length=5, blank=True)

    def __unicode__(self):
        return self.state_name

    class Meta:
        verbose_name_plural = 'State'


class PRV_Product_Category(models.Model):
    CATEGORY_STATUS = Choices((1, 'ACTIVE'), (0, 'INACTIVE'))
    category_name = models.CharField(max_length=100, blank=False)
    category_description = models.TextField(max_length=500, blank=True)
    category_active = models.IntegerField(choices=CATEGORY_STATUS, default=1)

    def __unicode__(self):
        return self.category_name

    class Meta:
        verbose_name_plural = 'PRV Product Category'


class Buyer(models.Model):
    BUYER_SUB_ALLOW = Choices((1, 'ALLOWED'), (0, 'NOT_ALLOWED'))
    FOR_FREE_STATE = Choices((1, 'YES'), (0, 'NO'))

    buyer_name = models.CharField(max_length=100, blank=False)
    login_link = models.CharField(max_length=400, blank=True)
    register_link = models.CharField(max_length=400, blank=True)
    buyer_intro = models.TextField(blank=True)
    for_free = models.IntegerField(choices=FOR_FREE_STATE, default=0)
    submission_allowed = models.IntegerField(choices=BUYER_SUB_ALLOW, default=1)
    buyer_state = models.ForeignKey(State)
    buyer_country = models.ForeignKey(Country)
    prod_category = models.ManyToManyField(PRV_Product_Category, related_name='buyer_prod_category')

    def __unicode__(self):
        return self.buyer_name

    class Meta:
        verbose_name_plural = 'Buyer'


class BuyerSellCode(models.Model):
    buyer = models.ForeignKey(Buyer)
    CATEGORY_OF_CODE_CHOICES = Choices("NAICS", "UNSPC", "SIC")
    category_of_code = models.CharField(choices=CATEGORY_OF_CODE_CHOICES, max_length=10)
    TYPE_OF_CODE_CHOICES = Choices("BUY", "SELL")
    type_of_code = models.CharField(choices=TYPE_OF_CODE_CHOICES, max_length=10)
    code = models.CharField(max_length=50, blank=False)

    def __unicode__(self):
        return self.code

    class Meta:
        verbose_name_plural = 'Buyer Sell Code'


class PaymentPlan(models.Model):
    plan_title = models.CharField(max_length=50, blank=True, null=True, unique=True)
    display_order = models.IntegerField(blank=True, null=True)
    cpc = models.DecimalField(max_digits=8, decimal_places=2, blank=True, default=0.0, verbose_name='Cost Per Corporation')
    active = models.BooleanField(default=0)

    def __unicode__(self):
        return self.plan_title

    class Meta:
        verbose_name_plural = 'PaymentPlan'

class PrvService(models.Model):
    service_name = models.CharField(max_length=50, blank=True, null=True)
    display_order = models.IntegerField(blank=True, null=True)
    is_active = models.BooleanField(default=0)
    
    def __unicode__(self):
        return self.service_name

class ServicePlan(models.Model):
    plan = models.ForeignKey(PaymentPlan)
    service = models.ForeignKey(PrvService)
    #cpc = models.DecimalField(max_digits=8, decimal_places=2, blank=True, default=0.0, verbose_name='Cost Per Corporation')
    is_active = models.BooleanField(default=1)
    
    def __unicode__(self):
        return self.plan.plan_title + " / " + self.service.service_name
    
class Supplier(models.Model):
    supplier_name = models.CharField(max_length=50)
    supplier_user = models.OneToOneField(User)
    
    STEP_STATE = (("REGISTERED","REGISTERED"), ("STEP1","STEP1"), ("STEP2","STEP2"), ("STEP3","STEP3"), ("COMPLETED","COMPLETED"))
    step_state = models.CharField(choices=STEP_STATE, max_length=20, default="REGISTERED")
    avatar = models.ImageField(upload_to='media/avatar', blank=True, null=True)

    def __unicode__(self):
        return self.supplier_name

    class Meta:
        verbose_name_plural = 'Supplier'

class SupplierInformation(models.Model):
    supplier = models.ForeignKey(Supplier)
    first_name = models.CharField(max_length=50, blank=True, null=True)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    position = models.CharField(max_length=50, blank=True, null=True)
    phone_number = models.IntegerField(max_length=15, blank=True, null=True)
    fax_number = models.IntegerField(max_length=20, blank=True, null=True)
    cell_number = models.IntegerField(max_length=20, blank=True, null=True)
    contact_email = models.EmailField(max_length=50, blank=True, null=True)
    contact_address = models.CharField(max_length=50,blank=True, null=True)
    city = models.CharField(max_length=20, blank=True, null=True)
    zip_code = models.CharField(max_length=20)
    username1 = models.CharField(max_length=50, blank=True, null=True)
    username2 = models.CharField(max_length=50, blank=True, null=True)
    username3 = models.CharField(max_length=50, blank=True, null=True)
    password = models.CharField(max_length=50, blank=True, null=True)
    
    def __unicode__(self):
        return self.first_name + " " + self.last_name
    

class RegisteredCorporationWithAlien(models.Model):
    supplier = models.ForeignKey(Supplier)
    corporation_name = models.CharField(max_length=50, blank=True, null=True)
    corporation_url = models.URLField(blank=True, null=True)
    corp_username = models.CharField(max_length=50, blank=True, null=True)
    corp_password = models.CharField(max_length=50, blank=True, null=True)
    
    def __unicode__(self):
        return self.corporation_name
     
class CouponCode(models.Model):
    code = models.CharField(max_length=50, unique=True)
    expire_date = models.DateField()
    active = models.BooleanField()
    DISCOUNT_TYPE = Choices("Percent", "Fixed rate")
    discount_type = models.CharField(max_length=30, choices=DISCOUNT_TYPE, default="Fixed rate", verbose_name="Discount Type")
    discount_value = models.IntegerField()
    is_used = models.BooleanField(default=0)

    def __unicode__(self):
        return self.code

    class Meta:
        verbose_name_plural = 'CouponCode'

class CouponValidity(models.Model):
    code = models.ForeignKey(CouponCode)
    plan = models.ForeignKey(PaymentPlan)
    PlanFrequency = models.CharField(max_length=30, choices=PLAN_FREQUENCY, default="monthly")

    def __unicode__(self):
        return self.code.code +"/"+self.plan.plan_title

    class Meta:
        verbose_name_plural = 'CouponValidity'
        
class Subscription(models.Model):
    supplier = models.ForeignKey(Supplier)
    #service_plan = models.ForeignKey(ServicePlan)
    prv_plan = models.ForeignKey(PaymentPlan)
    plan_frequency = models.CharField(max_length=50, blank=True, default="monthly", choices=PLAN_FREQUENCY)
    #coupon_code = models.ForeignKey(CouponCode)
    plan_amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    net_amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    SS = (("ACTIVE","Active"), ("NOT_SUBSCRIBED","Not Subscribed"),("PAYMENT_PENDING","Payment Pending"), ("EXPIRED","Expired"))
    subscription_status = models.CharField(choices=SS, max_length=20, default="NOT_SUBSCRIBED")
    subscription_date = models.DateField(blank=True, null=True)
    subscription_expire_date = models.DateField(blank=True, null=True)
    
    def __unicode__(self):
        return self.supplier.supplier_name + " / " + self.prv_plan.plan_title
    
    class Meta:
        verbose_name_plural = 'Supplier Subscription'
        unique_together = ("supplier", "prv_plan")
    
    
class SupplierSubmission(models.Model):
    supplier = models.ForeignKey(Supplier)
    buyer = models.ForeignKey(Buyer, related_name='submitted_buyer')
    submission_date = models.DateTimeField(blank=True, null=True)
    submission_status = models.CharField(max_length=50, default="pending", 
                        choices=(("pending","Pending"),("inprogress", "In Progress"),("submitted","Submitted")))
    submitted_user = models.CharField(max_length=20, blank=True, null=True)
    submitter_pass = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return self.supplier.supplier_name + " / " + self.buyer.buyer_name

    class Meta:
        verbose_name_plural = 'Supplier Submission'
        


#============== Models for supplier info page =======================

class Section(models.Model):
    section_name = models.CharField(max_length=25)
    display_order = models.IntegerField(unique=True)

    def __unicode__(self):
        return self.section_name

    class Meta:
        verbose_name_plural = 'Section'


class FieldMetaInfo(models.Model):
    field_name = models.CharField(max_length=50)
    section = models.ForeignKey(Section)
    FIELD_TYPE = Choices("Text", "URL", "Image", "Number", "Currency", "Date", "Checkbox", "Email", "Textarea", "Select", "Multi-select", "Radio",
                         "File")
    field_type = models.CharField(choices=FIELD_TYPE, max_length=25)
    max_length = models.IntegerField(blank=True, null=True)
    DEPENDENT = Choices((0, 'No'), (1, 'Yes'))
    is_dependent = models.IntegerField(choices=DEPENDENT, default=0)
    dependent_field = models.ForeignKey('self', null=True, blank=True)
    options = models.CharField(max_length=5000, blank=True)
    display_order = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.field_name

    class Meta:
        verbose_name_plural = 'FieldMetaInfo'


class BuyerAdditionalField(models.Model):
    field_name = models.CharField(max_length=50)

    section = models.ForeignKey(Section, blank=True)
    FIELD_TYPE = Choices("Text", "URL", "Image", "Number", "Currency", "Date", "Checkbox", "Email", "Textarea", "Select", "Multi-select", "Radio", "File")

    field_label = models.CharField(max_length=100, null=True, blank=False)
    section = models.ForeignKey(Section, blank=True, null=True)
    
    field_type = models.CharField(choices=FIELD_TYPE, max_length=25)
    max_length = models.IntegerField(blank=True, null=True)
    DEPENDENT = Choices((0, 'No'), (1, 'Yes'))
    is_dependent = models.IntegerField(choices=DEPENDENT, default=0)
    dependent_field = models.ForeignKey('self', null=True, blank=True)
    options = models.TextField(blank=True, null=True)
    display_order = models.IntegerField(blank=True, null=True)
    buyer = models.ForeignKey(Buyer)
    REQUIRED = Choices((0, 'No'), (1, 'Yes'))
    is_required = models.IntegerField(choices=REQUIRED, default=0)

    def __unicode__(self):
        return self.buyer.buyer_name + " / " + self.field_name

    class Meta:
        verbose_name_plural = 'BuyerAdditionalField'


class BuyerCommonField(models.Model):
    buyer = models.ForeignKey(Buyer)
    field = models.ForeignKey(FieldMetaInfo)
    REQUIRED = Choices((0, 'No'), (1, 'Yes'))
    is_required = models.IntegerField(choices=REQUIRED, default=0)

    def __unicode__(self):
        return self.buyer.buyer_name + "/" + self.field.field_name

    class Meta:
        verbose_name_plural = 'BuyerCommonField'

class AdditionalFileFieldData(models.Model):
    supplier = models.ForeignKey(Supplier)
    field = models.ForeignKey(BuyerAdditionalField)
    file_field = models.FileField(blank=True, verbose_name='Attachement',
                                           upload_to='attachment/extrafield/')

    def __unicode__(self):
        return self.field.field_name

    class Meta:
        verbose_name_plural = 'AdditionalFileFieldData'
        unique_together = ("supplier", "field")

class AdditionalFieldData(models.Model):
    supplier = models.ForeignKey(Supplier)
    field = models.ForeignKey(BuyerAdditionalField)
    field_value = models.CharField(max_length=200)

    def __unicode__(self):
        return self.field.field_name

    class Meta:
        verbose_name_plural = 'AdditionalFieldData'
        unique_together = ("supplier", "field")



class SupplierInfo(models.Model):
    supplier = models.OneToOneField(Supplier)
    #============ COMPANY INFORMATION ==============#
    COMP_NAME = models.CharField(max_length=200, blank=True, null=True, verbose_name='Legal Company Name', 
        validators=[validation_for_empty,validation_for_only_character])
    COMP_DBA_NAME = models.CharField(max_length=200, blank=True, null=True, verbose_name='DBA Company Name')
    COMP_FED_TAX = models.CharField(max_length=50, blank=True, null=True, verbose_name='Federal Tax ID')
    COMP_DUNN_STREET = models.CharField(max_length=50, blank=True, null=True, verbose_name='Dunn & Brad Street Number')

    #=========== ADDRESS SECTION ============#
    PHYSICAL_ADDRESS = models.CharField(max_length=500, blank=True,null=True, verbose_name='Physical Address')
    PHYSICAL_ADDRESS_CITY = models.CharField(max_length=500, blank=True,null=True, verbose_name='City')
    PHYSICAL_ADDRESS_STATE = models.CharField(max_length=50, blank=True,null=True, verbose_name='State')
    PHYSICAL_ADDRESS_COUNTY = models.CharField(max_length=50, blank=True,null=True, verbose_name='County')
    PHYSICAL_ADDRESS_COUNTRY = models.CharField(max_length=50, blank=True,null=True, verbose_name='Country')
    PHYSICAL_ADDRESS_ZIP = models.CharField(max_length=50, blank=True,null=True, verbose_name='Zip/Postal Code')

    REMITTANCE_ADDRESS = models.CharField(max_length=500, blank=True,null=True, verbose_name='Remittance Address')
    REMITTANCE_ADDRESS_CITY = models.CharField(max_length=500, blank=True,null=True, verbose_name='City')
    REMITTANCE_ADDRESS_STATE = models.CharField(max_length=50, blank=True,null=True, verbose_name='State')
    REMITTANCE_ADDRESS_COUNTY = models.CharField(max_length=50, blank=True,null=True, verbose_name='County')
    REMITTANCE_ADDRESS_COUNTRY = models.CharField(max_length=50, blank=True,null=True, verbose_name='Country')
    REMITTANCE_ADDRESS_ZIP = models.CharField(max_length=50, blank=True,null=True, verbose_name='Zip/Postal Code')

    MAILING_ADDRESS = models.CharField(max_length=500, blank=True, null=True, verbose_name='Mailing Address')
    MAILING_ADDRESS_CITY = models.CharField(max_length=500, blank=True,null=True, verbose_name='City')
    MAILING_ADDRESS_STATE = models.CharField(max_length=50, blank=True, null=True, verbose_name='State')
    MAILING_ADDRESS_COUNTY = models.CharField(max_length=50, blank=True,null=True, verbose_name='County')
    MAILING_ADDRESS_COUNTRY = models.CharField(max_length=50, blank=True,null=True, verbose_name='Country')
    MAILING_ADDRESS_ZIP = models.CharField(max_length=50, blank=True,null=True, verbose_name='Zip/Postal Code')

    COMP_PHONE = models.CharField(max_length=20, blank=True, null=True, verbose_name='Company Phone Number')
    COMP_FAX = models.CharField(max_length=20, blank=True,null=True, verbose_name='Company Fax Number')
    COMP_URL = models.CharField(max_length=100, blank=True,null=True, verbose_name='Company Website (URL)')

    #============== OWNER INFORMATION =============#
    OWNER1_NAME = models.CharField(max_length=100, blank=True, verbose_name='Owner 1 Name', null=True)
    OWNER1_TITLE = models.CharField(max_length=100, blank=True,null=True, verbose_name='Owner 1 Title')
    OWNER1_EMAIL = models.EmailField(blank=True, verbose_name='Owner 1 E-Mail')
    OWNER1_GENDER = models.CharField(max_length=10,null=True, blank=True, verbose_name='Owner 1 Gender')
    OWNER1_ETHNICITY = models.CharField(max_length=50,null=True, blank=True, verbose_name='Owner 1 Ethnicity')
    OWNER1_PERCENT_OWNERSHIP = models.CharField(max_length=3,blank=True, verbose_name='Owner 1 % Ownership', null=True)

    OWNER2_NAME = models.CharField(max_length=100, blank=True,null=True, verbose_name='Owner 2 Name')
    OWNER2_TITLE = models.CharField(max_length=100, blank=True,null=True, verbose_name='Owner 2 Title')
    OWNER2_EMAIL = models.EmailField(blank=True,null=True, verbose_name='Owner 2 E-Mail')
    OWNER2_GENDER = models.CharField(max_length=10,null=True, blank=True, verbose_name='Owner 2 Gender')
    OWNER2_ETHNICITY = models.CharField(max_length=50,null=True, blank=True, verbose_name='Owner 2 Ethnicity')
    OWNER2_PERCENT_OWNERSHIP = models.CharField(max_length=3,blank=True, verbose_name='Owner 2 % Ownership', 
        null=True)

    OWNER3_NAME = models.CharField(max_length=100,null=True, blank=True, verbose_name='Owner 3 Name')
    OWNER3_TITLE = models.CharField(max_length=100,null=True, blank=True, verbose_name='Owner 3 Title')
    OWNER3_EMAIL = models.EmailField(blank=True,null=True, verbose_name='Owner 3 E-Mail')
    OWNER3_GENDER = models.CharField(max_length=10,null=True, blank=True, verbose_name='Owner 3 Gender')
    OWNER3_ETHNICITY = models.CharField(max_length=50,null=True, blank=True, verbose_name='Owner 3 Ethnicity')
    OWNER3_PERCENT_OWNERSHIP = models.CharField(max_length=3, blank=True, verbose_name='Owner 3 % Ownership', null=True)

    #========= CONTACT INFORMATION =============#
    PRIMARY_CONT_NAME = models.CharField(max_length=100,null=True, blank=True, verbose_name='Primary Contact Name')
    PRIMARY_CONT_TITLE = models.CharField(max_length=50,null=True, blank=True, verbose_name='Job Title')
    PRIMARY_CONT_PHONE = models.CharField(max_length=20,null=True, blank=True, verbose_name='Phone Number')
    PRIMARY_CONT_PHONE_EXT = models.CharField(max_length=20,null=True, blank=True, verbose_name='Phone Ext.')
    PRIMARY_CONT_FAX = models.CharField(max_length=20,null=True, blank=True, verbose_name='Fax Number')
    PRIMARY_CONT_EMAIL = models.EmailField(blank=True,null=True, verbose_name='E-Mail Address')

    SECONDARY_CONT_NAME = models.CharField(max_length=100,null=True, blank=True, verbose_name='Secondary Contact Name')
    SECONDARY_CONT_TITLE = models.CharField(max_length=50,null=True, blank=True, verbose_name='Job Title')
    SECONDARY_CONT_PHONE = models.CharField(max_length=20,null=True, blank=True, verbose_name='Phone Number')
    SECONDARY_CONT_PHONE_EXT = models.CharField(max_length=20,null=True, blank=True, verbose_name='Phone Ext.')
    SECONDARY_CONT_FAX = models.CharField(max_length=20,null=True, blank=True, verbose_name='Fax Number')
    SECONDARY_CONT_EMAIL = models.EmailField(blank=True,null=True, verbose_name='E-Mail Address')

    #============= BUSINESS BIOGRAPHY ================#
    IS_COMP_PUBLICLY_TRADED = models.BooleanField(verbose_name='Is your company publicly traded?')
    STOCK_TICKET_ID = models.CharField(max_length=50,null=True, blank=True, verbose_name='Stock Ticket ID')
    NUM_OF_EMPLOYEES = models.CharField(max_length=10,null=True, blank=True, verbose_name='Number Of Employees')
    YEAR_BUSINESS_ESTABLISHED = models.CharField(max_length=4,null=True, blank=True, verbose_name='Year Business was Established')
    GEO_AREA_INTERNATIONAL = models.BooleanField(verbose_name='Geographical Service Area: International')
    GEO_AREA_NATIONAL = models.BooleanField(verbose_name='Geographical Service Area: National')
    GEO_AREA_LOCAL = models.BooleanField(verbose_name='Geographical Service Area: Local')
    GEO_AREA_REGIONAL = models.BooleanField(verbose_name='Geographical Service Area: Regional')
    ANNUAL_SALES_LASTYEAR = models.CharField(max_length=10, null=True, blank=True,
                                             verbose_name='Annual Sales For ' + str(date.today().year - 1))
    ANNUAL_SALES_TWOYEARSAGO = models.CharField(max_length=10, null=True, blank=True,
                                                verbose_name='Annual Sales For ' + str(date.today().year - 2))
    ANNUAL_SALES_THREEYEARSAGO = models.CharField(max_length=10, null=True, blank=True,
                                                  verbose_name='Annual Sales For ' + str(date.today().year - 3))
    PRODUCT_SERVICE_DESC = models.CharField(max_length=500, blank=True,null=True,
                                            verbose_name='Describe the Products/Services your company provides')

    #========== BUSINESS REFERENCES ==============#
    REF1_COMP_NAME = models.CharField(max_length=100, blank=True, verbose_name='Reference 1 Company Name')
    REF1_CONTACT_NAME = models.CharField(max_length=100, blank=True, verbose_name='Contact Name')
    REF1_SERVICES_PROVIDED = models.CharField(max_length=50, blank=True, verbose_name='Services Provided')
    REF1_PHONE = models.CharField(max_length=20, blank=True, verbose_name='Phone')
    REF1_EMAIL = models.EmailField(blank=True, verbose_name='Email')

    REF2_COMP_NAME = models.CharField(max_length=100,null=True, blank=True, verbose_name='Reference 2 Company Name')
    REF2_CONTACT_NAME = models.CharField(max_length=100,null=True, blank=True, verbose_name='Contact Name')
    REF2_SERVICES_PROVIDED = models.CharField(max_length=50,null=True, blank=True, verbose_name='Services Provided')
    REF2_PHONE = models.CharField(max_length=20, blank=True,null=True, verbose_name='Phone')
    REF2_EMAIL = models.EmailField(blank=True,null=True, verbose_name='Email')

    REF3_COMP_NAME = models.CharField(max_length=100,null=True, blank=True, verbose_name='Reference 3 Company Name')
    REF3_CONTACT_NAME = models.CharField(max_length=100,null=True, blank=True, verbose_name='Contact Name')
    REF3_SERVICES_PROVIDED = models.CharField(max_length=50,null=True, blank=True, verbose_name='Services Provided')
    REF3_PHONE = models.CharField(max_length=20, blank=True,null=True, verbose_name='Phone')
    REF3_EMAIL = models.EmailField(blank=True,null=True, verbose_name='Email')

    #========== E-BUSINESS READINESS ==========#
    HAS_ONLINE_CATALOG = models.CharField(max_length=3, blank=True,null=True,
                                          verbose_name='Does your company have an online catalog?')
    ACCEPTS_CREDIT_CARD = models.CharField(max_length=3, blank=True,null=True,
                                           verbose_name='Does your company accept payment by credit card?')
    IS_EDI_CAPABLE = models.CharField(max_length=3, blank=True,null=True,
                                      verbose_name='Is your company Electronic Data Interchange (EDI) capable?')

    #========= BUSINESS CLASSIFICATION =========#
    IS_SBE = models.NullBooleanField(verbose_name='Small Business Enterprise')
    IS_SBA_CCR_LISTED = models.CharField(max_length=3,null=True, blank=True, verbose_name='Are you SBA CCR listed')
    CAGE_CODE = models.CharField(max_length=20,null=True, blank=True, verbose_name='SBA CCR Cage Code')
    IS_8A = models.NullBooleanField(verbose_name='8(a)')
    IS_8A_CERTIFIED = models.CharField(max_length=3,null=True, blank=True, verbose_name='Is 8(a) certified by SBA')
    IS_8A_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='8(a): Expiration Date')
    IS_8A_CERT_NUMBER = models.CharField(max_length=50,null=True, blank=True, verbose_name='8(a): Certification No.')
    IS_8A_CERT_FILE = models.FileField(blank=True,null=True, verbose_name='8(a): Upload Certificate File', upload_to='attachment/')
    IS_SDB = models.BooleanField(verbose_name='Small Disadvantaged Business (SDB/SBA)')
    IS_SDB_CERTIFIED = models.CharField(max_length=3,null=True, blank=True, verbose_name='Is SDB certified by SBA')
    IS_SDB_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='SDB: Expiration Date')
    IS_SDB_CERT_NUMBER = models.CharField(max_length=50,null=True, blank=True, verbose_name='SDB: Certification No.')
    IS_SDB_CERT_FILE = models.FileField(blank=True,null=True, verbose_name='SDB: Upload Certificate File', upload_to='attachment/')
    IS_HUBZONE = models.NullBooleanField(verbose_name='HUBZone (SBA Certified)')
    IS_HUBZONE_CERTIFIED = models.CharField(max_length=3,null=True, blank=True, verbose_name='Is HUBZone certified by SBA')
    IS_HUBZONE_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='HUBZone: Expiration Date')
    IS_HUBZONE_CERT_NUMBER = models.CharField(max_length=50,null=True, blank=True, verbose_name='HUBZone: Certification No.')
    IS_HUBZONE_CERT_FILE = models.FileField(blank=True,null=True, verbose_name='HUBZone: Upload Certificate File',
                                            upload_to='attachment/')

    IS_MBE = models.BooleanField(verbose_name='Minority Business Enterprise')
    MBE_ETHNICITY = models.CharField(max_length=100,null=True, blank=True, verbose_name='MBE: Ethnicity')
    IS_MBE_CERTIFIED = models.CharField(max_length=3, blank=True,null=True,
                                        verbose_name='Is MBE Certified by a Certifying Agency?')
    IS_MBE_NMSDC_CERTIFIED = models.BooleanField(verbose_name='MBE: Is Certified by NMSDC?')
    MBE_NMSDC_CERT_AGENCY = models.CharField(max_length='200',null=True, blank=True, verbose_name='MBE NMSDC: Certifying Agency')
    MBE_NMSDC_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='MBE NMSDC: Expiration Date')
    MBE_NMSDC_CERT_NUMBER = models.CharField(max_length=50,null=True, blank=True, verbose_name='MBE NMSDC: Certification No.')
    MBE_NMSDC_CERT_FILE = models.FileField(blank=True,null=True, verbose_name='MBE NMSDC: Upload Certificate File',
                                           upload_to='attachment/')
    IS_MBE_OTHER_CERTIFIED = models.BooleanField(verbose_name='MBE: Is Certified by Other Organization?')
    MBE_OTHER_CERT_TYPE = models.CharField(max_length='100',null=True, blank=True, verbose_name='MBE Other: Certification Type')
    MBE_OTHER_CERT_AGENCY = models.CharField(max_length='200',null=True, blank=True, verbose_name='MBE Other: Certifying Agency')
    MBE_OTHER_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='MBE Other: Expiration Date')
    MBE_OTHER_CERT_NUMBER = models.CharField(max_length=50,null=True, blank=True, verbose_name='MBE Other: Certification No.')
    MBE_OTHER_CERT_FILE = models.FileField(blank=True,null=True, verbose_name='MBE Other: Upload Certificate File',
                                           upload_to='attachment/')

    IS_WBE = models.BooleanField(verbose_name='Woman Business Enterprise')
    IS_WBE_CERTIFIED = models.CharField(max_length=3,null=True, blank=True,
                                        verbose_name='Is WBE Certified by a Certifying Agency?')
    IS_WBE_WBENC_CERTIFIED = models.BooleanField(verbose_name='WBE: Is Certified by WBENC?')
    WBE_WBENC_CERT_AGENCY = models.CharField(max_length='200',null=True, blank=True, verbose_name='WBE WBENC: Certifying Agency')
    WBE_WBENC_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='WBE WBENC: Expiration Date')
    WBE_WBENC_CERT_NUMBER = models.CharField(max_length=50,null=True, blank=True, verbose_name='WBE WBENC: Certification No.')
    WBE_WBENC_CERT_FILE = models.FileField(blank=True, verbose_name='WBE WBENC: Upload Certificate File',
                                           upload_to='attachment/')
    IS_WBE_OTHER_CERTIFIED = models.BooleanField(verbose_name='WBE: Is Certified by Other Organization?')
    WBE_OTHER_CERT_TYPE = models.CharField(max_length='100',null=True, blank=True, verbose_name='WBE Other: Certification Type')
    WBE_OTHER_CERT_AGENCY = models.CharField(max_length='200',null=True, blank=True, verbose_name='WBE Other: Certifying Agency')
    WBE_OTHER_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='WBE Other: Expiration Date')
    WBE_OTHER_CERT_NUMBER = models.CharField(max_length=50,null=True, blank=True, verbose_name='WBE Other: Certification No.')
    WBE_OTHER_CERT_FILE = models.FileField(blank=True,null=True, verbose_name='WBE Other: Upload Certificate File',
                                           upload_to='attachment/')

    IS_VBE = models.BooleanField(verbose_name='Veteran Business Enterprise')
    IS_SDB_VETERAN = models.BooleanField(verbose_name='Service Disabled Veteran')
    IS_VBE_CERTIFIED = models.CharField(max_length=3, blank=True,null=True,
                                        verbose_name='Is VBE Certified by a Certifying Agency?')
    VBE_CERT_TYPE = models.CharField(max_length='100',null=True, blank=True, verbose_name='VBE: Certification Type')
    VBE_CERT_AGENCY = models.CharField(max_length='200',null=True, blank=True, verbose_name='VBE: Certifying Agency')
    VBE_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='VBE: Expiration Date')
    VBE_CERT_NUMBER = models.CharField(max_length=50,null=True, blank=True, verbose_name='VBE: Certification No.')
    VBE_CERT_FILE = models.FileField(blank=True, verbose_name='VBE: Upload Certificate File',
                                     upload_to='attachment/')

    IS_GLBTBE = models.BooleanField(verbose_name='GLBT Business Enterprise')
    IS_GLBTBE_CERTIFIED = models.CharField(max_length=3, blank=True,null=True,
                                           verbose_name='Is GLBTBE Certified by a Certifying Agency?')
    GLBTBE_CERT_TYPE = models.CharField(max_length='100', blank=True,null=True, verbose_name='GLBTBE: Certification Type')
    GLBTBE_CERT_TYPE_NAME = models.CharField(max_length='100', blank=True,null=True,
                                             verbose_name='GLBTBE: Certification Type Name')
    GLBTBE_CERT_AGENCY = models.CharField(max_length='200', blank=True, null=True,verbose_name='GLBTBE: Certifying Agency')
    GLBTBE_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='GLBTBE: Expiration Date')
    GLBTBE_CERT_NUMBER = models.CharField(max_length=50, blank=True,null=True, verbose_name='GLBTBE: Certification No.')
    GLBTBE_CERT_FILE = models.FileField(blank=True, null=True, verbose_name='GLBTBE: Upload Certificate File',
                                        upload_to='attachment/')

    IS_OTHER_DIVERSITY_CERTIFIED = models.BooleanField(verbose_name='Other Diversity Certification')
    OTHER_CERT_TYPE = models.CharField(max_length='100',null=True, blank=True, verbose_name='Other: Certification Type')
    OTHER_CERT_AGENCY = models.CharField(max_length='200',null=True, blank=True, verbose_name='Other: Certifying Agency')
    OTHER_CERT_EXPIRATION_DATE = models.DateField(blank=True, null=True, verbose_name='Other: Expiration Date')
    OTHER_CERT_NUMBER = models.CharField(max_length=50,null=True, blank=True, verbose_name='Other: Certification No.')
    OTHER_CERT_FILE = models.FileField(blank=True,null=True, verbose_name='Other: Upload Certificate File',
                                       upload_to='attachment/')

    PREPARER_NAME = models.CharField(max_length=100,null=True, blank=True, verbose_name="Preparer's Name")
    PREPARER_PHONE = models.CharField(max_length=20,null=True, blank=True, verbose_name="Preparer's Phone Number")
    PREPARER_EMAIL = models.EmailField(blank=True,null=True, verbose_name="Preparer's E-Mail Address")

    #========= EXTENDED PROFILE ============#
    COMP_LOGO = models.FileField(blank=True, null=True, verbose_name='Current Company Logo', upload_to='attachment/')
    COMP_PICTURE = models.FileField(blank=True, null=True, verbose_name='Current Company/Personal Picture',
                                    upload_to='attachment/')
    PS1_TITLE = models.CharField(max_length=100,null=True, blank=True, verbose_name='Name/Title')
    PS1_DESCRIPTION = models.CharField(max_length=500,null=True, blank=True, verbose_name='Description')
    PS1_UPLOAD_FILE = models.FileField(blank=True,null=True, verbose_name='Upload Attachment', upload_to='attachment/')
    
    PS2_TITLE = models.CharField(max_length=100, null=True,blank=True, verbose_name='Name/Title')
    PS2_DESCRIPTION = models.CharField(max_length=500,null=True, blank=True, verbose_name='Description')
    PS2_UPLOAD_FILE = models.FileField(blank=True,null=True, verbose_name='Upload Attachment', upload_to='attachment/')
    
    PS3_TITLE = models.CharField(max_length=100,null=True, blank=True, verbose_name='Name/Title')
    PS3_DESCRIPTION = models.CharField(max_length=500,null=True, blank=True, verbose_name='Description')
    PS3_UPLOAD_FILE = models.FileField(blank=True,null=True, verbose_name='Upload Attachment', upload_to='attachment/')
    
    PS4_TITLE = models.CharField(max_length=100,null=True, blank=True, verbose_name='Name/Title')
    PS4_DESCRIPTION = models.CharField(max_length=500,null=True, blank=True, verbose_name='Description')
    PS4_UPLOAD_FILE = models.FileField(blank=True,null=True, verbose_name='Upload Attachment', upload_to='attachment/')
    
    PS5_TITLE = models.CharField(max_length=100,null=True, blank=True, verbose_name='Name/Title')
    PS5_DESCRIPTION = models.CharField(max_length=500,null=True, blank=True, verbose_name='Description')
    PS5_UPLOAD_FILE = models.FileField(blank=True,null=True, verbose_name='Upload Attachment', upload_to='attachment/')

    def __unicode__(self):
        return self.supplier.supplier_name

    class Meta:
        verbose_name_plural = 'SupplierInfo'
        

class PaymentHistory(models.Model):
    supplier = models.ForeignKey(Supplier)
    plan = models.ForeignKey(PaymentPlan)
    plan_frequency = models.CharField(max_length=50, blank=True, null=True)
    plan_amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    net_amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    coupon = models.ForeignKey(CouponCode, blank=True, null=True)

    payment_id = models.CharField(max_length=100, blank=True, null=True)
    transaction = models.CharField(max_length=500, blank=True, null=True)
    payment_status = models.CharField(max_length=10, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    token = models.CharField(max_length=100, blank=True, null=True)

