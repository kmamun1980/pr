from django.views.generic.edit import FormView
from ssm.forms import AuthenticationForm, SupplierInfoForm, CustomRegistrationForm, SupplierInformationForm, RegisteredCorporationWithAlienForm
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import login, REDIRECT_FIELD_NAME
from prv3 import settings
from urlparse import urlparse, parse_qs
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from ssm.models import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.core import serializers
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from registration.views import RegistrationView
from django.contrib.sites.models import Site
from django import forms
from registration.models import RegistrationProfile
from registration.views import ActivationView as BaseActivationView
from registration.views import RegistrationView as BaseRegistrationView
from django.contrib.auth import logout, authenticate, login
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib import messages


from paypalrestsdk import Payment
import logging
import paypalrestsdk


def get_started(request):
    
    if request.user.is_authenticated():
        try:
            sp = Supplier.objects.get(supplier_user=request.user)
            subs = Subscription.objects.get(supplier=sp)
            old_plan = subs.prv_plan.plan_title
            old_frequency = subs.plan_frequency
            
        except Subscription.DoesNotExist:
            old_plan = None
            old_frequency = None
            pass

    total_suppliers = Supplier.objects.count()
        
    prv_services = PrvService.objects.filter(is_active=1).order_by("display_order")
    plans = PaymentPlan.objects.filter(active=1).order_by("display_order")
    
    plan_dict = {}
    plan_service_list = []
    service_names = []
    
    for srv in prv_services:
        service_names.append(srv.service_name)
        
    
    for plan in plans:
        svplan_list = []
        for service in prv_services:
            try:
                svplan = ServicePlan.objects.get(plan=plan, service=service, is_active=1)
                svplan_list.append(str(service.service_name))
            except ServicePlan.DoesNotExist:
                pass
        plan_dict = {
            'plan': plan.plan_title,
            'plan_id': plan.id,
            'services': svplan_list,
            'cpc': plan.cpc
        }
        
        plan_service_list.append(plan_dict)
        
    plan_frequency = ['Monthly', 'Quarterly', 'Half_Yearly', 'Yearly']
    
    
    if request.user.is_authenticated():
        return render_to_response("ssm/get_started_page.html", 
                                  {'plan_services': plan_service_list, 'frequencies': plan_frequency, 'old_frequency': old_frequency,
                                   'services': service_names, 'old_plan': old_plan, "total_suppliers": total_suppliers },
                                  context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect("/accounts/register/")
        # return render_to_response("ssm/get_started_page.html", 
        #       {'plan_services': plan_service_list, 'frequencies': plan_frequency, 'services': service_names},
        #         context_instance=RequestContext(request))

@login_required
def supplier_landing(request):
    
    return render_to_response("ssm/supplier_landing_page.html", {}, context_instance=RequestContext(request))


@login_required
@csrf_exempt
def save_plan(request):
    
    if request.method == 'POST':
        
        plan_id = request.POST['plan']
        sp = Supplier.objects.get(supplier_user=request.user)
        plan = PaymentPlan.objects.get(pk=plan_id)
        frequency = request.POST['frequency']
        try:
            subscription = Subscription.objects.get(supplier=sp)
            subscription.prv_plan = plan
            subscription.plan_frequency = frequency
            subscription.save()
            sp.step_state = "STEP1"
            sp.save()
            return HttpResponseRedirect("/ssm/buyer/")
        except Subscription.DoesNotExist:
            ''' Display flash message for successfully saved message'''
            subscription = Subscription.objects.create(supplier=sp, prv_plan=plan, plan_frequency=frequency, subscription_status="PAYMENT_PENDING")
            return HttpResponseRedirect("/ssm/buyer/")
#         except:
#             ''' Display flash message for resubmit'''
#             return HttpResponseRedirect("/ssm/get_started_page/")
        
    return HttpResponseRedirect("/")

@login_required
@csrf_exempt
def buy_now(request):
    form_data = request.POST['data']
    data_list = form_data.split("=")
    plan = data_list[0]
    plan_f = data_list[1]
    # plan_f = request.POST['plan_frequency']
    # plan = request.POST['plan']
    

    plan_details = PaymentPlan.objects.get(pk=plan)
    if plan_details.active:
        if plan_f == "Monthly":
            plan_amount = plan_details.monthly_registration
        elif plan_f == "Quarterly":
            plan_amount = plan_details.quarterly_registration
        elif plan_f == "Half_Yearly":
            plan_amount = plan_details.half_yearly_registration
        elif plan_f == "Yearly":
            plan_amount = plan_details.yearly_registration
        else:
            plan_amount = 0;
    else:
        return HttpResponse("NOTACTIVE")

    return HttpResponse(plan_amount)

@csrf_exempt
def check_coupon(request):
    
    plan_id = request.POST['plan']
    net_amount = request.POST['net_amount'][1:]
    plan_amount = request.POST['plan_amount'][1:]
    plan_frequency = request.POST['plan_f']
    code = request.POST['code']
    

    try:
        import datetime

        cp_code = CouponCode.objects.get(code=code)

        if cp_code.active != True:
            return HttpResponse("NotActive")

        if cp_code.expire_date < datetime.date.today():
            return HttpResponse("EXPIRED")


        plan1 = PaymentPlan.objects.get(pk=plan_id)

        cvs = cp_code.couponvalidity_set.filter(plan=plan1, code=cp_code)
        
        valid = False
        for cv in cvs:
            if cv.PlanFrequency == plan_frequency:
                valid = True

        if not valid:
            return HttpResponse("NotValid")

        

        if cp_code.discount_type == "Percent":
            net_amount = float(plan_amount) * (float(100 - cp_code.discount_value)/100)
            return HttpResponse(net_amount)
        elif cp_code.discount_type == "Fixed rate":
            net_amount = float(plan_amount) - cp_code.discount_value
            return HttpResponse(net_amount)

    except (ValueError, CouponCode.DoesNotExist):
        return HttpResponse("DoesNotExist")


@csrf_exempt
def create(request):
    plan_id = request.POST['plan_id']
    plan_frequesncy = request.POST['plan_f']
    plan_amount = request.POST['plan_amount']
    net_amount = request.POST['net_amount']
    coupon_code = request.POST['cp_code']
    coupon_amount = request.POST['cp_amount']
    
    plan = PaymentPlan.objects.get(pk=plan_id)
    #coupon = CouponCode.objects.get(code=coupon_code)
    sp = Supplier.objects.get(supplier_user=request.user)
    
    paypalrestsdk.configure({
        "mode": "sandbox", 
        "client_id": "AXD1xxBNG1VP1KO9j3PRAJmibtCGuNN7Zjry-5rIeE1UBZMjBEq6EqUiesOv",
        "client_secret": "EBINrBDb5Xl_jdkw2QeiwhLhN5TOZwFWQNOwhlbiyldIptJ8SxYtANUr3FHJ" 
    })
    site = request.META['HTTP_HOST']
    payment = paypalrestsdk.Payment({
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
        "return_url": "http://"+ site +"/payment/execute",
        "cancel_url": "http://"+ site +"/" },
        "transactions": [{
        "item_list": {
        "items": [{
            "name": plan.plan_title,
            "sku": plan_id,
            "price": net_amount,
            "currency": "USD",
            "quantity": 1 }]},
            "amount": {
            "total": net_amount,
            "currency": "USD" },
            "description": "This is the payment transaction description." 
        }]
    })

    if payment.create():
        print("Payment[%s] created successfully" % (payment.id))
        ph = PaymentHistory(supplier=sp, plan=plan, plan_frequency=plan_frequesncy, plan_amount=plan_amount,
                   net_amount=net_amount)
        ph.payment_id = payment.id
        ph.payment_status = payment.state
        ph.payer_id = payment.create_time
        ph.create_time = payment.create_time
        
        # Redirect the user to given approval url
        for link in payment.links:
            if link.method == "REDIRECT":
                redirect_url = link.href
                query = parse_qs(urlparse(redirect_url).query)
                token = query['token'][0]
                ph.token = token
                ph.save()
        
        return redirect(redirect_url)
    else:
        print("Error while creating payment:")
        print(payment.error)
    return HttpResponseRedirect("/payment/error/")

def execute(request):
  
    payer_id = request.GET['PayerID']
    token = request.GET['token']
    
    sp = Supplier.objects.get(supplier_user=request.user)
    ph = PaymentHistory.objects.get(token=token, supplier=sp)
    
    paypalrestsdk.configure({
        "mode": "sandbox", # sandbox or live
        "client_id": "AXD1xxBNG1VP1KO9j3PRAJmibtCGuNN7Zjry-5rIeE1UBZMjBEq6EqUiesOv",
        "client_secret": "EBINrBDb5Xl_jdkw2QeiwhLhN5TOZwFWQNOwhlbiyldIptJ8SxYtANUr3FHJ" 
    })
    
    payment = paypalrestsdk.Payment.find(ph.payment_id)
    
    if payment.execute({"payer_id": payer_id}):
        print("Payment execute successfully")
        ph.payment_status = 'completed'
        ph.save()
    else:
        print(payment.error) # Error Hash
    messages.success(request, 'Your payment is successfully loaded.')
    return HttpResponseRedirect("/ssm/buyer/")

''' Following views is for /ssm/buyer/ =============='''

@login_required
def searchPage(request):
    
    try:
        sp = Supplier.objects.get(supplier_user=request.user)
        if sp.step_state == "REGISTERED":
            ''' Show flash message '''
            return HttpResponseRedirect("/ssm/get_started/")
    except (ValueError, Supplier.DoesNotExist):
        ''' If no supplier account for this user send an email to admin '''
        return HttpResponseRedirect("/ssm/get_started/")

    categories = PRV_Product_Category.objects.all()
    saved_buyers = []
    
    ssubmissions = sp.suppliersubmission_set.all()
    total_suppliers = Supplier.objects.count()
    if ssubmissions:
        for ssubmission in ssubmissions:
            saved_buyers.append(ssubmission.buyer)
    # import pdb; pdb.set_trace()

    return render_to_response('ssm/search_corporation.html',{"total_suppliers": total_suppliers ,'categories': categories, 'saved_buyers': saved_buyers},
        context_instance=RequestContext(request))


'''
@csrf_exempt
def ajaxlogin(request):
    
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return HttpResponse('logged')
    else:
        return HttpResponse('error')


    

'''
def test_drive(request):
    categories = PRV_Product_Category.objects.all()
    total_suppliers = Supplier.objects.count()
    return render_to_response('test_drive.html',{'categories': categories},
        context_instance=RequestContext(request))
   
 
@csrf_exempt
def testSearchBuyers(request):
    saved_buyers = []
    buyers_list = []
    final_buyers_list = []
    
#     showall = request.POST['showall']

    if len(request.POST['findbuyer']) > 0:
        findbuyers = request.POST['findbuyer'].split(", ")
    else:
        findbuyers = None

    if len(request.POST['findcat']) > 0:
        findcats = request.POST['findcat'].split(", ")
    else:
        findcats = None

    #====== filtering buyers for categories ===========

    clist = []    
    if findcats:
        for cat in findcats:
            catq1 = PRV_Product_Category.objects.filter(category_name__icontains=cat).distinct()
            for cq in catq1:
                clist.append(cq.pk)

    if len(request.POST['categories']) > 0:
        categories = request.POST['categories']
        cats = categories.split("&")

        for cat in cats:
            cid = cat.split("=")[1]
            clist.append(cid)
    
    buyers_from_cat = Buyer.objects.filter(prod_category__in=clist).distinct()
    for br in buyers_from_cat:
        buyers_list.append(br)

    #====== filtering buyers for manual  input ===========
    no_buyer_found = []

    if findbuyers:
        for findbuyer in findbuyers:
            try:
                fbuyers = Buyer.objects.filter(buyer_name__icontains=findbuyer).distinct()
                for fb in fbuyers:
                    buyers_list.append(fb)
            except (ValueError, Buyer.DoesNotExist):
                no_buyer_found.append(findbuyer)
    
    final_buyers_list = buyers_list[0:5]
    return render_to_response('testdrive/test_searched_buyers.html',
        {'buyers': final_buyers_list, 'no_buyer_found':no_buyer_found }, context_instance=RequestContext(request))
    #return HttpResponse(json)
'''
@csrf_exempt
def prvRegister(request):
    from django.db import IntegrityError

    username = request.POST['username2']
    password = request.POST['password']
    email = request.POST['email']
    if Site._meta.installed:
        site = Site.objects.get_current()
    else:
        site = RequestSite(request)
    try:
        new_user = RegistrationProfile.objects.create_inactive_user(username, email, password, site)
        signals.user_registered.send(sender="admin@test.com", user=new_user, request=request)
        msg = "<h2>An email to "+email+" has been sent for active your account."
        return HttpResponse(msg)
    except IntegrityError, e:
        #return render_to_response("template.html", {"message": e.message})
        return HttpResponse("2")
    
    


class RgisterFormView(RegistrationView):
    form_class = CustomRegistrationForm
    success_url = "/"

    def register(self, request, **cleaned_data):
        username = cleaned_data['username']
        password = cleaned_data['password1']
        email = cleaned_data['email']
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)
        new_user = RegistrationProfile.objects.create_inactive_user(username, email,
                                                                    password, site)
        signals.user_registered.send(sender=self.__class__,
                                     user=new_user,
                                     request=request)
        # from notifications.views import send_email_notification
        # send_email_notification(settings.REGISTER_TEMPLATE, [new_user])

        return HttpResponseRedirect("/")
    
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect("/prv/supplier_profile/")
        return super(RgisterFormView, self).get(request, *args, **kwargs)



def handle_uploaded_file(f):
    with open('', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
'''


@login_required
def supplierProfile(request):
    try:
        
        sp = Supplier.objects.get(supplier_user=request.user)
        if sp.step_state == "REGISTERED":
            ''' Show flash message '''
            return HttpResponseRedirect("/ssm/get_started/")
        elif sp.step_state == "STEP1":
            ''' Show flash message '''
            return HttpResponseRedirect("/ssm/buyer/")

    except (ValueError, Supplier.DoesNotExist):
        return HttpResponseRedirect("/ssm/get_started/")

    commonfields = {}
    additionalfields = []
    adfieldsdata = []
    saved_buyers = []
    sections = {}
    total_suppliers = Supplier.objects.count()
    ssubmissions = sp.suppliersubmission_set.all()
    
    for ssb in ssubmissions:
        saved_buyers.append(ssb.buyer)
    

    for buyer in saved_buyers:
        cfs = buyer.buyercommonfield_set.all()
        for cf in cfs:
            if cf.field.field_name not in commonfields:
                commonfields[cf.field.field_name] = cf
        adfields = buyer.buyeradditionalfield_set.all()
        additionalfields += adfields
        
    try:
        spi = SupplierInfo.objects.get(supplier=sp)
        sp_form = SupplierInfoForm(instance=spi)

    except (ValueError, SupplierInfo.DoesNotExist):
        spi = None
        sp_form = SupplierInfoForm()
    from django.core import validators
    
    if request.method == "POST":
        # import pdb; pdb.set_trace()
        commondata = {}
        extradata = {}

        # for k in commonfields.keys():
        #     commondata[k] = ", ".join(request.POST.getlist(k))


        if spi:
            sp_form = SupplierInfoForm(request.POST, request.FILES, instance=spi)
        else:
            sp_form = SupplierInfoForm(request.POST, request.FILES)

        if sp_form.is_valid():
            spf = sp_form.save(commit=False)

            spf.supplier = sp
            spf.save()

            for adf in additionalfields:
                
                if adf.field_type != 'File':
                    fv = request.POST[str(adf.pk)]
                    try:
                        md = AdditionalFieldData.objects.get(supplier=sp, field=adf)
                        md.field_value = fv
                        md.save()
                    except (ValueError, AdditionalFieldData.DoesNotExist):
                        md = AdditionalFieldData.objects.create(supplier=sp, field=adf, field_value=fv)

                else:
                    
                    try:
                        md = AdditionalFileFieldData.objects.get(supplier=sp, field=adf)
                        fv = request.FILES[adf.field_name]
                        if not fv:
                            clear = request.POST[adf.field_name+"-clear"]                       
                        
                        md.file_field = fv
                        md.save()
                    except (ValueError, AdditionalFileFieldData.DoesNotExist):
                        fv = request.FILES[adf.field_name]
                        md = AdditionalFileFieldData.objects.create(supplier=sp, field=adf, file_field=fv)
                    except MultiValueDictKeyError:
                        try:
                            clear = request.POST[adf.field_name+"-clear"]
                            md = AdditionalFileFieldData.objects.get(supplier=sp, field=adf)
                            md.delete()
                        except MultiValueDictKeyError:
                            pass         
            messages.success(request, 'Your profile was updated.')
            return HttpResponseRedirect("/")
        else:
            for k,v in sp_form.errors.items():
                messages.error(request, v[0])
            for k, v in sp_form.fields.items():
            
                if k not in commonfields.keys():
                    del sp_form.fields[k]
                else:               
                    if commonfields[k].is_required:
                        v.required = True

                    sec = commonfields[k].field.section
                    v.display_order = commonfields[k].field.display_order
                    v.section = sec.section_name
                    if sec.section_name not in sections.values():
                        sections[sec.display_order] = sec.section_name


                    v.type = commonfields[k].field.field_type
                    v.options = commonfields[k].field.options.split(", ")
                    # CH = ()
                    # for option in v.options:
                    #     CH = CH + ((option, option),)
                    CH = ((u'Dhaka', u'Dhaka'), (u'Sylhet', u'Sylhet'), (u'Chitagong', u'Chitagong'), (u'Khulna', u'Khulna'), (u'Rajshahi', u'Rajshahi'))
                    if v.type == 'Multi-select':
                        #import pdb; pdb.set_trace()
                        v.widget.choices = CH
                    elif v.type == 'Textarea':
                        v.value = getattr(spi, k)

                    v.max_length = commonfields[k].field.max_length
            
            #import pdb; pdb.set_trace()
            for additionalfield in additionalfields:
                if additionalfield.field_type == 'File':
                    try:
                        adfilefield = AdditionalFileFieldData.objects.get(supplier=sp, field=additionalfield)
                        additionalfield.data_value = adfilefield.file_field.url

                    except (ValueError, AdditionalFileFieldData.DoesNotExist):
                        pass
                else:
                    try:
                        adfdata = AdditionalFieldData.objects.get(supplier=sp, field=additionalfield)
                        additionalfield.data_value = adfdata.field_value
                    except (ValueError, AdditionalFieldData.DoesNotExist):
                        pass

                sections[additionalfield.section.display_order] = additionalfield.section.section_name
                if additionalfield.options:
                    additionalfield.options = additionalfield.options.split(", ")

            return render_to_response("ssm/supplier_profile_page.html", {"total_suppliers":total_suppliers, 'form': sp_form}, context_instance=RequestContext(request))

    else:
        
        for k, v in sp_form.fields.items():
            
            if k not in commonfields.keys():
                del sp_form.fields[k]
            else:               
                if commonfields[k].is_required:
                    v.required = True

                sec = commonfields[k].field.section
                v.display_order = commonfields[k].field.display_order
                v.section = sec.section_name
                if sec.section_name not in sections.values():
                    sections[sec.display_order] = sec.section_name


                v.type = commonfields[k].field.field_type
                v.options = commonfields[k].field.options.split(", ")
                # CH = ()
                # for option in v.options:
                #     CH = CH + ((option, option),)
                CH = ((u'Dhaka', u'Dhaka'), (u'Sylhet', u'Sylhet'), (u'Chitagong', u'Chitagong'), (u'Khulna', u'Khulna'), (u'Rajshahi', u'Rajshahi'))
                if v.type == 'Multi-select':
                    #import pdb; pdb.set_trace()
                    v.widget.choices = CH
                    
                elif v.type == 'Textarea':
                    v.value = getattr(spi, k)


                v.max_length = commonfields[k].field.max_length
        
        
        for additionalfield in additionalfields:
            if additionalfield.field_type == 'File':
                try:
                    adfilefield = AdditionalFileFieldData.objects.get(supplier=sp, field=additionalfield)
                    additionalfield.data_value = adfilefield.file_field.url

                except (ValueError, AdditionalFileFieldData.DoesNotExist):
                    pass
            else:
                try:
                    adfdata = AdditionalFieldData.objects.get(supplier=sp, field=additionalfield)
                    additionalfield.data_value = adfdata.field_value
                except (ValueError, AdditionalFieldData.DoesNotExist):
                    pass

            sections[additionalfield.section.display_order] = additionalfield.section.section_name
            if additionalfield.options:
                additionalfield.options = additionalfield.options.split(", ")

    EXTRAFIELD = None
    for k,v in sections.items():
        if v == 'EXTRAFIELD':
            EXTRAFIELD = True
            del sections[k]

    
    return render_to_response("ssm/supplier_profile_page.html", 
        {"total_suppliers":total_suppliers, 'form': sp_form, "supplier": sp, 'adfields': additionalfields, 'sections': sections, 'EXTRAFIELD':EXTRAFIELD}, 
        context_instance=RequestContext(request))




def getCategories(request):

    term = request.GET.get('term')
    cats = PRV_Product_Category.objects.filter(category_name__contains=term)

    datacat = []
    for cat in cats:
        data = '\"'+cat.category_name+'\",'
        datacat.append(data)

    return HttpResponse(datacat)

'''
@csrf_exempt
def prvLogin(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return render_to_response("logged_in_user.html", {}, context_instance=RequestContext(request))
                
            else:
                messagewarning(request, 'Your account is not active')
        else:
            messages.error(request, 'Invalid username and password')



class LoginView(FormView):
    template_name = 'registration/login.html'
    form_class = AuthenticationForm
    redirect_field_name = REDIRECT_FIELD_NAME
    success_url = "/prv/supplier_profile/"

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(LoginView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        
        #self.check_and_delete_test_cookie()
        
        login(self.request, form.get_user())
        
        return super(LoginView, self).form_valid(form)

    # def form_invalid(self, form):
    #     self.set_test_cookie()
    #     return super(LoginView, self).form_invalid(form)

    # def get_success_url(self):
    #     if self.success_url:
    #         redirect_to = self.success_url
    #     else:
    #         redirect_to = self.request.REQUEST.get(self.redirect_field_name, '')

    #     netloc = urlparse.urlparse(redirect_to)[1]
    #     if not redirect_to:
    #         redirect_to = settings.LOGIN_REDIRECT_URL
    #     # Security check -- don't allow redirection to a different host.
    #     elif netloc and netloc != self.request.get_host():
    #         redirect_to = settings.LOGIN_REDIRECT_URL
    #     return redirect_to

    # def set_test_cookie(self):
    #     self.request.session.set_test_cookie()

    # def check_and_delete_test_cookie(self):
    #     if self.request.session.test_cookie_worked():
    #         self.request.session.delete_test_cookie()
    #         return True
    #     return False

    def get(self, request, *args, **kwargs):
        #self.set_test_cookie()
        if request.user.is_authenticated():
            return HttpResponseRedirect("/prv/buyer/")
        return super(LoginView, self).get(request, *args, **kwargs)

'''

@login_required
@csrf_exempt
def searchBuyers(request):
    saved_buyers = []
    buyers_list = []
    final_buyers_list = []
    sp = Supplier.objects.get(supplier_user=request.user)
    ssubmissions = sp.suppliersubmission_set.all()

    if ssubmissions:
        for ssubmission in ssubmissions:
            saved_buyers.append(ssubmission.buyer)

    showall = request.POST['showall']

    if len(request.POST['findbuyer']) > 0:
        findbuyers = request.POST['findbuyer'].split(", ")
    else:
        findbuyers = None

    if len(request.POST['findcat']) > 0:
        findcats = request.POST['findcat'].split(", ")
    else:
        findcats = None

    #====== filtering buyers for categories ===========

    clist = []    
    if findcats:
        for cat in findcats:
            catq1 = PRV_Product_Category.objects.filter(category_name__contains=cat).distinct()
            for cq in catq1:
                clist.append(cq.pk)

    if len(request.POST['categories']) > 0:
        categories = request.POST['categories']

        cats = categories.split(", ")

        for cat in cats:
            #cid = cat.split("=")[1]
            clist.append(cat)
    
    buyers_from_cat = Buyer.objects.filter(prod_category__in=clist).distinct()
    for br in buyers_from_cat:
        buyers_list.append(br)

    #====== filtering buyers for manual  input ===========
    no_buyer_found = []

    if findbuyers:
        for findbuyer in findbuyers:
            try:
                fbuyers = Buyer.objects.filter(buyer_name__contains=findbuyer).distinct()
                for fb in fbuyers:
                    if fb not in buyers_list:
                        buyers_list.append(fb)
            except (ValueError, Buyer.DoesNotExist):
                no_buyer_found.append(findbuyer)
    

    if showall == 'no':

        
        for bl in buyers_list:
            if bl not in saved_buyers:
                final_buyers_list.append(bl)
    else:
        final_buyers_list = buyers_list

    total_suppliers = Supplier.objects.count()
    return render_to_response('ssm/searched_buyers.html',
        {'buyers': final_buyers_list, 'no_buyer_found':no_buyer_found, "total_suppliers":total_suppliers },context_instance=RequestContext(request))
    #return HttpResponse(json)

@login_required
@csrf_exempt
def saveSelectedBuyers(request):
    import datetime
    from django.utils.timezone import utc

    if len(request.POST['selectedbuyers']) > 0:

        #===== get all selected buyers============
        buyer_list = []
        buyers = request.POST['selectedbuyers']
        buyerslist = buyers.split("&")
        for br in buyerslist:
            bid = br.split("=")[1]
            buyerobj = Buyer.objects.get(pk=bid)
            buyer_list.append(buyerobj)

        #========= get supplier ===================
        
        sp = Supplier.objects.get(supplier_user=request.user)

        #========= get saved supplier if any ====== 
        saved_buyers = []        
        ssubmissions = sp.suppliersubmission_set.all()
        if ssubmissions:
            for ssubmission in ssubmissions:
                saved_buyers.append(ssubmission.buyer)
        
        
        duplicate_buyers = []
        added_buyers = []
        sdate = datetime.datetime.utcnow().replace(tzinfo=utc)

        for buyer in buyer_list:
            
            if buyer not in saved_buyers:
                ssb = SupplierSubmission(supplier=sp, buyer=buyer)
                ssb.save()
                added_buyers.append(buyer)
            else:
                duplicate_buyers.append(buyer)
        
        saved_buyers = saved_buyers + added_buyers
        return render_to_response('ssm/selected_buyers.html',
                {'saved_buyers': saved_buyers, 'duplicate_buyers': duplicate_buyers}, 
                context_instance=RequestContext(request))
        
    else:
        return HttpResponse('No')


def submission_status(request):
    sp = Supplier.objects.get(supplier_user=request.user)
    ssubmissions = sp.suppliersubmission_set.all()
    total_suppliers = Supplier.objects.count()
    return render_to_response('ssm/submission_status.html',
                {'submissions': ssubmissions, "total_suppliers": total_suppliers}, 
                context_instance=RequestContext(request))

@login_required  
@csrf_exempt
def deletebuyer(request):
    
    bid = request.POST['bid']
    buyer = Buyer.objects.get(pk=bid)
    bname = buyer.buyer_name
    usr = request.user
    sp = Supplier.objects.get(supplier_user=usr)
    ssb = SupplierSubmission.objects.get(supplier=sp, buyer=buyer)
    ssb.delete()
    saved_buyers = []        
    ssubmissions = sp.suppliersubmission_set.all()
    if ssubmissions:
        for ssubmission in ssubmissions:
            saved_buyers.append(ssubmission.buyer)

    return render_to_response('ssm/selected_buyers.html',
                {'saved_buyers': saved_buyers})


@login_required  
@csrf_exempt
def deletebuyer2(request):
    
    bid = request.POST['bid']
    buyer = Buyer.objects.get(pk=bid)
    bname = buyer.buyer_name
    usr = request.user
    sp = Supplier.objects.get(supplier_user=usr)
    ssb = SupplierSubmission.objects.get(supplier=sp, buyer=buyer)
    ssb.delete()
    saved_buyers = []        
    ssubmissions = sp.suppliersubmission_set.all()
    if ssubmissions:
        for ssubmission in ssubmissions:
            saved_buyers.append(ssubmission.buyer)

    return render_to_response('ssm/buyers_list.html',
                {'buyers_list': saved_buyers}, context_instance=RequestContext(request))






@login_required  
@csrf_exempt
def addmanualbuyer(request):
    
    if len(request.POST['bname']) > 0:    
        bname = request.POST['bname']
        #bname_list = bname.split(", ")
        buyer = Buyer.objects.filter(buyer_name__contains=bname)
        if buyer.count() == 0:

            usr = request.user
            sp = Supplier.objects.get(supplier_user=usr)


    return HttpResponse(bname)

def my_logout(request):
    logout(request)
    return HttpResponseRedirect("/")


@login_required
def payment(request):
#     import pdb; pdb.set_trace()
    sp = Supplier.objects.get(supplier_user=request.user)
    total_buyers = sp.suppliersubmission_set.count()
    ss = sp.suppliersubmission_set.all()
    buyers_list = []
    for s in ss:
        buyers_list.append(s.buyer)
    subscription = Subscription.objects.get(supplier=sp)
    plan_frequency = subscription.plan_frequency
    plan = subscription.prv_plan
    total_bill = plan.cpc * total_buyers
    return render_to_response('ssm/payment.html', 
            {'supplier': sp, 'total_buyers': total_buyers, 'plan': plan, 'plan_frequency': plan_frequency,
            'buyers_list': buyers_list, "total_bill": total_bill }, context_instance=RequestContext(request))

@csrf_exempt
def payment_create(request):
  plan_id = request.POST['plan_id']
  plan_frequesncy = request.POST['plan_f']
  
  net_amount = request.POST['net_amount']
  quantity = request.POST['total_buyers']
  coupon_code = request.POST['cp_code']
#   coupon_amount = request.POST['cp_amount']

  plan = PaymentPlan.objects.get(pk=plan_id)
  cpc = int(plan.cpc)
  if coupon_code:
      coupon = CouponCode.objects.get(code=coupon_code)
  sp = Supplier.objects.get(supplier_user=request.user)

  paypalrestsdk.configure({
    "mode": "sandbox", 
    "client_id": "AXOwHhDSd4Y0GVbYXGrmMSSDKTUKh07SHEsBsW2Y6slxfNXiiXNqShJ9eisV",
    "client_secret": "EHIM0hCcyq51Lxl5npvgCYn44ENKugljlZtW8x_0xolZAU76QQ4frNfSOo5r" 
  })
  site = request.META['HTTP_HOST']
  payment = paypalrestsdk.Payment({
    "intent": "sale",
    "payer": {
      "payment_method": "paypal"},
    "redirect_urls": {
      "return_url": "http://"+ site +"/ssm/payment/execute/",
      "cancel_url": "http://"+ site +"/" },
    "transactions": [{
      "item_list": {
        "items": [{
          "name": plan.plan_title,
          "sku": plan_id,
          "price": cpc,
          "currency": "USD",
          "quantity": quantity }]},
      "amount": {
        "total": net_amount,
        "currency": "USD" },
      "description": "This is the payment transaction description." 
    }]
  })
  
  if payment.create():
    print("Payment[%s] created successfully"%(payment.id))
    ph = PaymentHistory(supplier=sp, plan=plan, plan_frequency=plan_frequesncy,
                     net_amount=net_amount)
    ph.payment_id = payment.id
    ph.payment_status = payment.state
    ph.payer_id = payment.create_time
    ph.create_time = payment.create_time
    
    # Redirect the user to given approval url
    for link in payment.links:
        if link.method == "REDIRECT":
          redirect_url = link.href
          query = parse_qs(urlparse(redirect_url).query)
          token = query['token'][0]
          ph.token = token
          ph.save()
        
          return redirect(redirect_url)
  else:
    print("Error while creating payment:")
    print(payment.error)
    return HttpResponseRedirect("/payment/error/")

@login_required
def payment_execute(request):
  payer_id = request.GET['PayerID']
  token = request.GET['token']

  sp = Supplier.objects.get(supplier_user=request.user)
  ph = PaymentHistory.objects.get(token=token, supplier=sp)
  subscription = Subscription.objects.get(supplier=sp)
  paypalrestsdk.configure({
  "mode": "sandbox", # sandbox or live
  "client_id": "AXOwHhDSd4Y0GVbYXGrmMSSDKTUKh07SHEsBsW2Y6slxfNXiiXNqShJ9eisV",
  "client_secret": "EHIM0hCcyq51Lxl5npvgCYn44ENKugljlZtW8x_0xolZAU76QQ4frNfSOo5r" })
  
  payment = paypalrestsdk.Payment.find(ph.payment_id)

  if payment.execute({"payer_id": payer_id}):
    print("Payment execute successfully")
    ph.payment_status = 'completed'
    ph.save()
    sp.step_state = "STEP2"
    sp.save()
    subscription.subscription_status = "ACTIVE"
    subscription.save()
  else:
    print(payment.error) # Error Hash
  messages.success(request, 'Your payment is successfully loaded.')
  return HttpResponseRedirect("/ssm/supplier_profile/")
  # return render_to_response("payment/payment_successfull.html", {"supplier": sp, 'ph': ph }, 
  #       context_instance=RequestContext(request))
  #return HttpResponse("Your payer id is %s"%payer_id)

@login_required
def payment_history(request):
  pass
@login_required
def payment_success(request):
  return render_to_response("/ssm/payment/payment_successfull.html", {}, 
         context_instance=RequestContext(request))

@csrf_exempt
def supplier_info(request):
    sp = Supplier.objects.get(supplier_user=request.user)
    form = SupplierInformationForm()
    corps = RegisteredCorporationWithAlien.objects.filter(supplier=sp)
    alian_form = RegisteredCorporationWithAlienForm()
    total_suppliers = Supplier.objects.count()
    if request.method == "POST":
        form = SupplierInformationForm(request.POST)
        if form.is_valid():
            si = form.save(commit=False)
            si.supplier = sp
            si.save()
            return HttpResponseRedirect("/ssm/supplier_profile/")
        else:
            return render_to_response("ssm/supplier_info.html", {"total_suppliers":total_suppliers, 'form': form, 'corps': corps, 'alian_form': alian_form }, context_instance=RequestContext(request))
    
#     import pdb; pdb.set_trace();
    return render_to_response("ssm/supplier_info.html", {"total_suppliers":total_suppliers, 'form': form, 'corps': corps,'alian_form': alian_form }, 
         context_instance=RequestContext(request))

@csrf_exempt
def add_corporation(request):
    corp_name = request.POST['corp_name']
    corp_url = request.POST['corp_url']
    username = request.POST['username']
    password = request.POST['password']
    sp = Supplier.objects.get(supplier_user=request.user)
    try:
        corp = RegisteredCorporationWithAlien.objects.create(supplier=sp,
                                                          corporation_name=corp_name, 
                                                          corporation_url=corp_url, 
                                                          corp_username=username, 
                                                          corp_password=password)
    except:
        pass
    
    return render_to_response("ssm/coporations_list.html", {'corp': corp}, 
                              context_instance=RequestContext(request))
    
    
    
    
    
    