from django.conf.urls import patterns, include, url
#from ssm.views import searchPage


urlpatterns = patterns('',
    #url(r'^$', 'PRV2_project.views.home', name='home'),
    
    url(r'^get_started/$', 'ssm.views.get_started', name='get_started'),
    url(r'^supplier_landing/$', 'ssm.views.supplier_landing', name='supplier_landing'),
    url(r'^buynow/$', 'ssm.views.buy_now', name='buy_now'),
    url(r'^save_plan/$', 'ssm.views.save_plan', name='save_plan'),
    url(r'^check_coupon/$', 'ssm.views.check_coupon', name='check_coupon'),
# 
# 
     url(r'^test_drive/$', 'ssm.views.test_drive', name='test_drive'),
     url(r'^test_search_buyers/$', 'ssm.views.testSearchBuyers', name='test_search_buyers'),
#     # url for search buyers page
     url(r'^buyer/$', 'ssm.views.searchPage', name='buyer_page'),
     url(r'^search_buyers/$', 'ssm.views.searchBuyers', name='search_buyers'),
     url(r'^save_buyers/$', 'ssm.views.saveSelectedBuyers', name='save_buyers'),
     url(r'^delete_buyer/$', 'ssm.views.deletebuyer', name='delete_buyer'),
     url(r'^delete_buyer2/$', 'ssm.views.deletebuyer2', name='delete_buyer2'),
     url(r'^add_manual_buyer/$', 'ssm.views.addmanualbuyer', name='add_manual_buyer'),
# 
#     # url for supplier profile page
     url(r'^supplier_profile/$', 'ssm.views.supplierProfile', name='supplier_profile'),
#     url(r'^test_ajax/$', 'ssm.views.ajaxlogin', name='ajaxlogin'),
    url(r'^supplier_info/$', 'ssm.views.supplier_info', name='supplier_info'),
    url(r'^add_corporation/$', 'ssm.views.add_corporation', name='add_corporation'),
    url(r'^submission_status/$', 'ssm.views.submission_status', name='submission_status'),
    
    url(r'^payment/$', 'ssm.views.payment', name='payment_create'),
    url(r'^payment/create/$', 'ssm.views.payment_create', name='payment_create'),
    url(r'^payment/execute/$', 'ssm.views.payment_execute', name='payment_execute'),
#     url(r'^payment/history/$', 'ssm.views.payment_history', name='payment_history'),
#     url(r'^payment/success/$', 'ssm.views.payment_success', name='payment_success'),
 

)