from django.contrib import admin
from ssm.models import *
from django import forms


class BuyerSellCodeInLine(admin.TabularInline):
    model = BuyerSellCode
    extra = 1


class BuyerCommomFieldInLine(admin.TabularInline):
    model = BuyerCommonField
    extra = 1


class BuyerAdditionalFieldModelForm(forms.ModelForm):
    options = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = BuyerAdditionalField


class BuyerAdditionalFieldAdmin(admin.ModelAdmin):
    list_display = ['field_name', 'field_label', 'buyer']
    list_filter = ['buyer']
    form = BuyerAdditionalFieldModelForm


class BuyerAdditionalFieldInLine(admin.TabularInline):
    model = BuyerAdditionalField
    extra = 1


class BuyerAdmin(admin.ModelAdmin):
    inlines = [BuyerSellCodeInLine, BuyerCommomFieldInLine, BuyerAdditionalFieldInLine]


class SupplierSubmissionInLine(admin.StackedInline):
    model = SupplierSubmission
    extra = 0


class SupplierAdmin(admin.ModelAdmin):
    inlines = [SupplierSubmissionInLine]


class SectionAdmin(admin.ModelAdmin):
    list_display = ['section_name', 'display_order']


class FieldMetaInfoModelForm(forms.ModelForm):
    options = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = FieldMetaInfo


class FieldMetaInfoAdmin(admin.ModelAdmin):
    list_display = ['field_name', 'section', 'field_type']
    list_filter = ['section']
    form = FieldMetaInfoModelForm

admin.site.register(Country)
admin.site.register(State)
admin.site.register(PRV_Product_Category)

admin.site.register(Buyer, BuyerAdmin)
#admin.site.register(BuyerSellCode)
admin.site.register(Supplier, SupplierAdmin)
admin.site.register(SupplierSubmission)

# models for supplier info page
admin.site.register(AdditionalFileFieldData)
admin.site.register(Section, SectionAdmin)
admin.site.register(FieldMetaInfo, FieldMetaInfoAdmin)
admin.site.register(BuyerAdditionalField, BuyerAdditionalFieldAdmin)
admin.site.register(BuyerCommonField)
admin.site.register(AdditionalFieldData)
admin.site.register(SupplierInfo)
admin.site.register(PaymentPlan)
admin.site.register(PaymentHistory)
admin.site.register(CouponCode)
admin.site.register(CouponValidity)
admin.site.register(Subscription)
admin.site.register(PrvService)
admin.site.register(ServicePlan)

