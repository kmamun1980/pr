from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    #url(r'^$', 'PRV2_project.views.home', name='home'),
    
    url(r'^create/$', 'payment.views.create', name='payment_create'),
    url(r'^execute/$', 'payment.views.execute', name='payment_execute'),
    url(r'^history/$', 'payment.views.history', name='payment_history'),
    url(r'^success/$', 'payment.views.success', name='success'),
 

)

