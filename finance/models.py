from django.db import models
from model_utils import Choices
#from django_fsm.db.fields import FSMField, transition, can_proceed
from ssm.models import *
# Create your models here.


class Subscription(models.Model):
    supplier = models.ForeignKey(Supplier)
    plan = models.ForeignKey(PaymentPlan)
    plan_frequesncy = models.CharField(max_length=50, blank=True, null=True)
    coupon_code = models.ForeignKey(CouponCode)
    plan_amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    net_amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    SUBSCRIPTION_STATUS = Choices("PENDING", "ACTIVE", "EXPIRED")
    subscription_status = models.CharField(choices=SUBSCRIPTION_STATUS, max_length=10, blank=True, null=True)
    expire_date = models.DateField()


class PaymentHistory(models.Model):
    supplier = models.ForeignKey(Supplier)
    plan = models.ForeignKey(PaymentPlan)
    plan_frequency = models.CharField(max_length=50, blank=True, null=True)
    plan_amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    net_amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    coupon = models.ForeignKey(CouponCode, blank=True, null=True)

    payment_id = models.CharField(max_length=100, blank=True, null=True)
    transaction = models.CharField(max_length=500, blank=True, null=True)
    payment_status = models.CharField(max_length=10, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    token = models.CharField(max_length=100, blank=True, null=True)

