import os
import sys
sys.stdout = sys.stderr

import site
site.addsitedir('/var/ve/prv2/lib/python2.7/site-packages')

sys.path.append('/var/djapp/PRV3')

os.environ['DJANGO_SETTINGS_MODULE'] = 'prv3.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
