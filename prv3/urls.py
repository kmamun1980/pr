from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'prv3.views.home', name='home'),
    url(r'^aboutus/$', 'prv3.views.aboutus', name='aboutus'),
    url(r'^contactus/$', 'prv3.views.contactus', name='contactus'),
    
    
    # url(r'^prv2/', include('prv2.foo.urls')),
    url(r'^ssm/', include('ssm.urls')),
    #url(r'^finance/', include('finance.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^accounts/', include('userManager.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
