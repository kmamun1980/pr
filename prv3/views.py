from django.template import RequestContext
from django.shortcuts import render_to_response
from ssm.models import Supplier

def home(request):
    hostname = request.get_host()
    total_suppliers = Supplier.objects.count()
    return render_to_response('homepage/home.html',{'hostname': hostname, "total_suppliers": total_suppliers }, context_instance=RequestContext(request))

def aboutus(request):
	total_suppliers = Supplier.objects.count()
	return render_to_response("aboutus.html",{"total_suppliers": total_suppliers },context_instance=RequestContext(request))

def contactus(request):
	total_suppliers = Supplier.objects.count()
	return render_to_response("contactus.html",{"total_suppliers": total_suppliers },context_instance=RequestContext(request))