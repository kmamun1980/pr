from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    url(r'^login/$',"userManager.views.login", name="login"),
    url(r'^logout/$',"userManager.views.logout", name="logout"),
    url(r'^user_not_active/$',"userManager.views.user_not_active", name="user_not_active"),
#     url(r'^loggedin/$',"userManager.views.logged_in", name="logged_in"),
#     url(r'^invalid/$',"userManager.views.invalid_login", name="invalid_login"),
    #url(r'^loggedout/$',"userManager.views.logged_out", name="logged_out"),
    
    url(r'^register/$',"userManager.views.register", name="register"),
    url(r'^register/(?P<activation_code>[\w]+)/active/$',"userManager.views.register_activate", name="register"),
    url(r'^register/success/$',"userManager.views.register_success", name="register_success"),
    url(r'^activation/invalid/$',"userManager.views.activation_invalid", name="activation_invalid"),
    
    url(r'^forget_password/$',"userManager.views.forget_password", name="forget_password"),
    url(r'^password/(?P<activation_code>[\w]+)/reset/$',"userManager.views.password_reset", name="password_reset"),
    url(r'^password_reset_done/$',"userManager.views.password_reset_done", name="password_reset_done"),
)