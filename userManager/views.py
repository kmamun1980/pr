from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib import auth, messages
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from forms import RegistrationForm, PasswordResetForm
from userManager.models import ActivationCode
from templated_email import send_templated_mail
from ssm.models import *
from django.contrib.auth.views import password_reset
import uuid
from md5 import md5
from templated_email import send_templated_mail
from django.contrib.sites.models import Site

@csrf_exempt
def login(request):
    if request.method == "POST":
        username = request.POST.get("username","")
        password = request.POST.get("password","")
        user = auth.authenticate(username=username, password=password)
        
        if user is not None:
            if user.is_active and not user.is_staff:
                auth.login(request, user)
                try:
                    sp = Supplier.objects.get(supplier_user=user)
                except (Supplier.DoesNotExist):
                    return HttpResponseRedirect("/ssm/get_started/")
                
                if sp.step_state == 'REGISTERED':
#                    return HttpResponseRedirect("/ssm/supplier_landing/")
                    return HttpResponseRedirect("/ssm/get_started/")
                elif sp.step_state == 'STEP1' or sp.step_state == 'COMPLETED':
                    return HttpResponseRedirect("/ssm/buyer/")
                elif sp.step_state == 'STEP2':
                    return HttpResponseRedirect("/ssm/supplier_profile/")
                else:
                    return HttpResponseRedirect("/ssm/supplier_landing/")
            elif user.is_active and user.is_staff:
                auth.login(request, user)
                return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/accounts/user_not_active/")
        else:
            messages.success(request, 'No user with the give credentials')
            return HttpResponseRedirect("/accounts/login/")
    else:
        c = {}
        c.update(csrf(request))
        return render_to_response("usermanager/login.html", c, context_instance=RequestContext(request))

def user_not_active(request):
    return render_to_response("usermanager/user_not_active.html",{})

def logged_in(request):
    return render_to_response("usermanager/loggedin.html", {"full_name": request.user.username})
    
def invalid_login(request):
    return render_to_response("usermanager/invalid_login.html")

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/")

def register(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/accounts/register/success")
    
    args = {}
    args.update(csrf(request)) 
    args['form'] = RegistrationForm()
    print args
    return render_to_response("usermanager/register.html", args, context_instance=RequestContext(request))

def register_activate(request, activation_code):
    try:
        ac = ActivationCode.objects.get(code=activation_code)
        if ac and ac.status == 0:
            user = ac.user
            user.is_active = True
            user.save()
            #ac.status = 1
            ac.delete()
            try:
                sp = Supplier.objects.create(supplier_name=user.username, supplier_user=user)
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                auth.login(request, user)
                return HttpResponseRedirect("/ssm/get_started/")
                
            except:
                ''' Send an email to admin for creating an supplier for this user'''
                pass
            return HttpResponseRedirect("/accounts/login/")
    except ActivationCode.DoesNotExist:
        return HttpResponseRedirect("/accounts/activation/invalid/")    


def activation_invalid(request):
    return render_to_response("usermanager/activation_invalid.html", {}, context_instance=RequestContext(request))
    
def register_success(request):
    return render_to_response("usermanager/registration_success.html",{}, context_instance=RequestContext(request))

def forget_password(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        try:
            user = User.objects.get(email=email)
            
            activation_code = md5(str(uuid.uuid4())).hexdigest()
            current_site = Site.objects.get_current()
            HOSTNAME = current_site.domain
            url = HOSTNAME + "/accounts/password/" + activation_code + "/reset/"
            
            send_templated_mail(
                    template_name='password_reset_link',
                    from_email='hello@cloudcustomsolutions.com',
                    recipient_list=[user.email,],
                    context={
                        'activation_url': url,
                    },
                )
            ActivationCode.objects.create(user=user,code=activation_code, status=0, code_for="PASSWORD_RESET")
            return HttpResponseRedirect("/")
        except User.DoesNotExist:
            ''' Flash message user does not exist'''
            pass

def password_reset(request, activation_code):
    try:
        
        code = activation_code
        ac = ActivationCode.objects.get(code=code)
        user = ac.user
        form = PasswordResetForm()
        return render_to_response("usermanager/password_reset.html",{'form': form, 'user_id': user.pk}, context_instance=RequestContext(request))
    except ActivationCode.DoesNotExist:
        return HttpResponseRedirect("/")
    
@csrf_exempt
def password_reset_done(request):
    
    if request.method == "POST":
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            password = request.POST["password"]
            user_id = request.POST["user_id"]
            user = User.objects.get(pk=user_id)
            user.set_password(password)
            user.save()
        else:
            return render_to_response("usermanager/password_reset.html",{'form': form}, 
                                      context_instance=RequestContext(request))
            
    ''' show flash message '''
    return HttpResponseRedirect("/")
        
        
    