from django import forms
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.mail import send_mail
from templated_email import send_templated_mail
from userManager.models import ActivationCode
from django.contrib.sites.models import Site
import uuid
from md5 import md5

class RegistrationForm(UserCreationForm):
    
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
    def save(self, commit=True):
        
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.set_password(self.cleaned_data['password1'])
        user.is_active = False
        #testmail = send_mail("registration", "activation link", "mamun1980@gmail.com", [user.email,], fail_silently=False)
        
        activation_code = md5(str(uuid.uuid4())).hexdigest()
        
        current_site = Site.objects.get_current()
        
        HOSTNAME = current_site.domain
        url = "http://localhost:8000/accounts/register/" + activation_code + "/active/"
            
        if commit:
            
            try:
                user.save()
                send_templated_mail(
                    template_name='registration_confirmation',
                    from_email='hello@cloudcustomsolutions.com',
                    recipient_list=[user.email,],
                    context={
                        'activation_url':url,
                    },
                )
                ActivationCode.objects.create(user=user,code=activation_code, status=0)
            except:
                pass
        return user

class PasswordResetForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)