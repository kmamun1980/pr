from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class ActivationCode(models.Model):
    user = models.ForeignKey(User)
    code = models.CharField(max_length=200, unique=True)
    ACTIVATION_STATUS = ((1,"ACTIVE"), (2,"INACTIVE"), (0,"PENDING"))
    status = models.IntegerField(choices=ACTIVATION_STATUS)
    CODE_FOR = (("PASSWORD_RESET","PASSWORD_RESET"), ("USER_REGISTER","USER_REGISTER"))
    code_for = models.CharField(max_length=20, choices=CODE_FOR, blank=True, null=True)