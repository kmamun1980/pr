# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ActivationCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=200)),
                ('status', models.IntegerField(choices=[(1, b'ACTIVE'), (2, b'INACTIVE'), (0, b'PENDING')])),
                ('code_for', models.CharField(blank=True, max_length=20, null=True, choices=[(b'PASSWORD_RESET', b'PASSWORD_RESET'), (b'USER_REGISTER', b'USER_REGISTER')])),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
