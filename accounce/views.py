# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from prv.models import *
from payment.models import *
from urlparse import urlparse, parse_qs
from django.http import HttpResponseRedirect

from paypalrestsdk import Payment
import logging
import paypalrestsdk

@csrf_exempt
def create(request):
  
  plan_id = request.POST['plan_id']
  plan_frequesncy = request.POST['plan_f']
  plan_amount = request.POST['plan_amount']
  net_amount = request.POST['net_amount']
  coupon_code = request.POST['cp_code']
  coupon_amount = request.POST['cp_amount']

  plan = PaymentPlan.objects.get(pk=plan_id)
  #coupon = CouponCode.objects.get(code=coupon_code)
  sp = Supplier.objects.get(supplier_user=request.user)

  paypalrestsdk.configure({
    "mode": "sandbox", 
    "client_id": "AXD1xxBNG1VP1KO9j3PRAJmibtCGuNN7Zjry-5rIeE1UBZMjBEq6EqUiesOv",
    "client_secret": "EBINrBDb5Xl_jdkw2QeiwhLhN5TOZwFWQNOwhlbiyldIptJ8SxYtANUr3FHJ" 
  })
  site = request.META['HTTP_HOST']
  payment = paypalrestsdk.Payment({
    "intent": "sale",
    "payer": {
      "payment_method": "paypal"},
    "redirect_urls": {
      "return_url": "http://"+ site +"/payment/execute",
      "cancel_url": "http://"+ site +"/" },
    "transactions": [{
      "item_list": {
        "items": [{
          "name": plan.plan_title,
          "sku": plan_id,
          "price": net_amount,
          "currency": "USD",
          "quantity": 1 }]},
      "amount": {
        "total": net_amount,
        "currency": "USD" },
      "description": "This is the payment transaction description." 
    }]
  })

  if payment.create():
    print("Payment[%s] created successfully"%(payment.id))
    ph = PaymentHistory(supplier=sp, plan=plan, plan_frequency=plan_frequesncy, plan_amount=plan_amount,
                       net_amount=net_amount)
    ph.payment_id = payment.id
    ph.payment_status = payment.state
    ph.payer_id = payment.create_time
    ph.create_time = payment.create_time
    
    # Redirect the user to given approval url
    for link in payment.links:
      if link.method == "REDIRECT":
        redirect_url = link.href
        query = parse_qs(urlparse(redirect_url).query)
        token = query['token'][0]
        ph.token = token
        ph.save()

        return redirect(redirect_url)
  else:
    print("Error while creating payment:")
    print(payment.error)
    return HttpResponseRedirect("/payment/error/")


def execute(request):
  
  payer_id = request.GET['PayerID']
  token = request.GET['token']

  sp = Supplier.objects.get(supplier_user=request.user)
  ph = PaymentHistory.objects.get(token=token, supplier=sp)
  
  paypalrestsdk.configure({
  "mode": "sandbox", # sandbox or live
  "client_id": "AXD1xxBNG1VP1KO9j3PRAJmibtCGuNN7Zjry-5rIeE1UBZMjBEq6EqUiesOv",
  "client_secret": "EBINrBDb5Xl_jdkw2QeiwhLhN5TOZwFWQNOwhlbiyldIptJ8SxYtANUr3FHJ" })
  
  payment = paypalrestsdk.Payment.find(ph.payment_id)

  if payment.execute({"payer_id": payer_id}):
    print("Payment execute successfully")
    ph.payment_status = 'completed'
    ph.save()
  else:
    print(payment.error) # Error Hash
  messages.success(request, 'Your payment is successfully loaded.')
  return HttpResponseRedirect("/")
  # return render_to_response("payment/payment_successfull.html", {"supplier": sp, 'ph': ph }, 
  #       context_instance=RequestContext(request))
  #return HttpResponse("Your payer id is %s"%payer_id)

def history(request):
  pass

def success(request):
  return render_to_response("payment/payment_successfull.html", {}, 
         context_instance=RequestContext(request))

